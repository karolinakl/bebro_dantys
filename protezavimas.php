<!DOCTYPE html>
<html>
<head>	
	<title>Protezavimas</title>

	<?php include "virsutiniai.php"; ?>
</head>
<body class="taktai">
<?php include "header.php"; ?>

<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li><a href="paslaugos.php">Paslaugos</a></li>
  <li>Protezavimas</li>
</ul>

<h1 class="antraste">Dantų protezavimas</h1>
<div class="container ">


<div class="row">
    <div class="col s12 m12 l7">
    	<img class="responsive-img" src="img/Implantu-foto-1-jpg.jpg" alt="Dantprotezai">
    <!-- 	<img class="responsive-img" src="img/protezai.jpg" alt="Dantys po plombavimo"> -->
	</div>
	<div class="col s12 m12 l5">
		<p class="content-info">Protezavimas – tai danties kietųjų audinių anatominės formos ir spalvos atkūrimas, kai dantis dėl įgimtos ar įgytos patologijos suyra ir jo negalima atkurti plombinėmis medžiagomis. Dantys taip pat protezuojami esant patologiniam dantų nusidėvėjimui, po periodontologinių operacijų ar ortodontinio gydymo, dantų formos ir kontaktų atstatymui.</p>
	</div>
</div>

<h3>Protezavimo tipai</h3>
<hr class="linija">
<br>
 <ul class="collapsible">
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Protezavimas ant dantų</div>
      <div class="collapsible-body content-info-small"><span>Dantų protezavimas paprastai atliekamas esant didesniems dantų audinių pažeidimams kuomet nuosavo danties nebegalima atkurti plombinėmis medžiagomis ar estetinio plombavimo būdu.
Protezuoti galima vieną, kelis ar net visus dantis. Šis dantų restauravimo būdas tinka tiek priekinių, šoninių ar krūminių dantų srityje.</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Protezavimas ant implantų</div>
      <div class="collapsible-body content-info-small"><span>Protezavimas antrasis implantacijos etapas, kurio metu pagaminama matoma danties dalis (vainikėlis). Laboratorijoje individuliai kiekvienam pacientui yra gaminami nepriekaištingos formos, dydžio ir spalvos dantų vainikėliai, kurie pasižymi ne tik gražiu ir natūraliu estetiniu vaizdu, bet ir tarnauja kaip nuosavi dantys.</span></div>
    </li>
</div>

<?php include "footer.php"; ?>
</body>
</html>