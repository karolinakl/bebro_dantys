<div class="navbar-fixed">
		<nav>
			<div "container">
				<nav id= virsus>
					<div class="nav-wrapper">
						<a href="index.php" class="brand-logo"><img id="logo" src="img/dantu_logo.jpeg"></a>
						<a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
						<ul class="right hide-on-med-and-down">
							<li><a href="index.php">Apie mus</a></li>							
							<div class="dropdownbut">				
								<li><a class="dropbtnas" href="paslaugos.php">Paslaugos</a></li>
								<div class="dropdown-contentas">
							    	<a href="terapinis_gydymas.php">Terapinis gydymas</a>
							    	<a href="chirurginis_gydymas.php">Chirurginis gydymas</a>
							    	<a href="burnos_higiena.php">Burnos higiena</a>
							    	<a href="protezavimas.php">Protezavimas</a>
							  	</div>
							</div>	
							<li><a href="komanda.php">Mūsų komanda</a></li>
							<li><a href="kontaktai.php">Kontaktai</a></li>					
						</ul>
					</div>
				</nav>
			</div>
		</nav>
	</div>
	<ul class="sidenav" id="mobile-demo">
		<li><a href="index.php">Apie mus</a></li>
		<li><a href="paslaugos.php">Paslaugos</a></li>
		<li><a href="komanda.php">Mūsų komanda</a></li>
		<li><a href="kontaktai.php">Kontaktai</a></li>
	</ul>

<a id="skambinti" class="btn tooltipped" data-position="left" data-tooltip="Paskambinkite mums +370xxxxxxxx" href="tel:+370xxxxxxxx"><i class="material-icons">call</i></a>
<a id="registruotis" class="btn tooltipped" data-position="left" data-tooltip="Registruokites apžiūrai" href="kontaktai.php"><i class="material-icons">content_paste</i></a>
<a id="ivertinimas" class="btn tooltipped" data-position="left" data-tooltip="Įvertinkite klinikos darbą" href="vertinimas.php"><i class="material-icons">rate_review</i></a>



