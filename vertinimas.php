<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">


  <title>Bebro dantys</title>

	<?php include "virsutiniai.php"; 	


  include ".git/conect.php";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if (isset($_GET["pasitenkinimas"]) && $_GET["pasitenkinimas"] != "") {
  
    $sql = "INSERT INTO paslaugu_vertinimas (id, pasitenkinimas)
    VALUES (null, '" . $_GET["pasitenkinimas"] . "')";
      if (mysqli_query($conn, $sql))  {
          echo "Duomenys išsaugoti"; 
          header("Location: vertinimas.php?uzregistruota=ok");
      } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
      }
}   
?>
</head>

<body class = "taktai">

<?php include "header.php"; ?>

<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li>Vetinimas</li>
</ul>

<div class="container">

<h1 class = "antraste-be-fono"> Klinikos vertinimas</h1>
<!-- 
<form action="vertinimas.php">
  <p>Kaip vertinate mūsų paslaugas?</p>
  <input type="radio" name="pasitenkinimas" value="5"> Labai gerai (5)<br>
  <input type="radio" name="pasitenkinimas" value="4"> Gerai (4)<br>
  <input type="radio" name="pasitenkinimas" value="3"> Patenkinimai (3)<br> 
   <input type="radio" name="pasitenkinimas" value="2"> Prastai (2)<br> 
   <input type="radio" name="pasitenkinimas" value="1"> Labai prastai (1)<br> 
  <input type="submit" value="Submit">
</form> -->

<div class="row">

  <div class="col s12 m6 l6 " >

    <form action="vertinimas.php" >
      <div align=justif>
      <p class="content-info">Kaip vertinate mūsų paslaugas?</p>
        <p>
          <label>
            <input name="pasitenkinimas" type="radio" value="5"/>
            <span>Labai gerai (5)</span>
          </label>
        </p>
        <p>
          <label>
            <input name="pasitenkinimas" type="radio" value="4"/>
            <span>Gerai (4)</span>
          </label>
        </p>
        <p>
          <label>
            <input name="pasitenkinimas" type="radio" value="3" />
            <span>Patenkinimai (3)</span>
          </label>
        </p>
        <p>
          <label>
            <input name="pasitenkinimas" type="radio"value="2"/>
            <span>Prastai (2)</span>
          </label>
        </p>
        <p>
          <label>
            <input name="pasitenkinimas" type="radio" value="1"/>
            <span>Labai prastai (1)</span>
          </label>
        </p>
        <button class="waves-effect waves-light btn-large index_mygtukai" type="siusti">Išsaugoti</button>
        </div>
      </form>
  </div>

  <div class="col s12 m6 l6 " >
      <br>
      <br>
      <div id="myPieChart"></div>
  </div>
</div>
  <br>



<?php
$sql = "SELECT pasitenkinimas, COUNT(*) AS balsu_kiekis FROM paslaugu_vertinimas 
    GROUP BY pasitenkinimas";

$result = mysqli_query($conn, $sql);
?>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(drawChart);

   function drawChart() {
      // Define the chart to be drawn.
      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Element');
      data.addColumn('number', 'Percentage');
      data.addRows([

        <?php 
        if (mysqli_num_rows($result) > 0) {

              while($row = mysqli_fetch_assoc($result)) {

                echo "['" . $row['pasitenkinimas'] . "', " . $row['balsu_kiekis'] . "],";
              }
          }
          mysqli_close($conn);
        ?>
      ]);

      // Instantiate and draw the chart.
      var chart = new google.visualization.PieChart(document.getElementById('myPieChart'));
      chart.draw(data, null);
    }
  </script>


</div>

<?php include "footer.php"; ?>


</body>
</html