<!DOCTYPE html>
<html>
<head>	
	<title>Paslaugos</title>

	<?php include "virsutiniai.php"; ?>
</head>
<body class="taktai">
<?php include "header.php"; ?>
	
<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li>Paslaugos</li>
</ul>

<div class="parallax-container">
	      <div class="parallax gamta"><img src="img/irankiai.jpg"></div>
</div>

<div class="container">
<h1 class="skyriaus-antraste";">Bebro dantų paslaugos</h1>

<div class="content-info">
	<p>Mieli klientai, odontologijos klinika "Bebro dantys" teikia visapusiškas dantų ir burnos priežiūros paslaugas. Čia jums tiek išvalysime dantis, tiek pakeisime juos naujais - viskas jūsų patogumui ir gerai savijautai. </p>

	<p>Išsamiau apie kiekvieną teikiamą paslaugą sužinosite paspaudę ant jos.</p>
</div>

<hr class="linija">
<br>

<div class="row">

    <div class="col s12 m6 l6 " >
        <div class="card">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="img/what_happens_to_extracted_teeth.jpg">
    </div>
    <div class="card-content taktai">
      <span class="card-title activator grey-text text-darken-4">Chirurginis gydymas<i class="material-icons right">more_vert</i></span>
      <p><a href="chirurginis_gydymas.php">Plačiau</a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title">Chirurginis gydymas<i class="material-icons right">close</i></span>
      <p>Burnos chirurgija- specializuota odontologijos šaka, kurios metu burnos ertmėje atliekamos įvairios chirurginės procedūros.</p>
    </div>
    </div>
    </div>

<div class="col s12 m6 l6 " >
        <div class="card">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="img/dentist-teeth-polishing.jpg">
    </div>
    <div class="card-content taktai">
      <span class="card-title activator grey-text text-darken-4">Burnos higiena<i class="material-icons right">more_vert</i></span>
      <p><a href="burnos_higiena.php">Plačiau</a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title">Burnos higiena<i class="material-icons right">close</i></span>
      <p>Nepriekaištinga burnos higiena yra pagrindinė priemonė, norint išvengti dantų ėduonies bei burnos ertmės gleivinės ir periodonto ligų.</p>
    </div>
    </div>
 </div>

 </div>

<div class="row">


<div class="col s12 m6 l6 " >
        <div class="card">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="img/sedation-dentistry-825x550.jpg">
    </div>
    <div class="card-content taktai">
      <span class="card-title activator grey-text text-darken-4">Terapinis gydymas<i class="material-icons right">more_vert</i></span>
      <p><a href="terapinis_gydymas.php">Plačiau</a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title">Terapinis gydymas<i class="material-icons right">close</i></span>
      <p>Terapinis dantų gydymas (plombavimas) – tai profilaktinių ir gydymo priemonių taikymas, siekiant išvengti, atstatyti kariozinės ar nekariozinės kilmės dantų defektus.</p>
    </div>
    </div>
    </div>

   

    <div class="col s12 m6 l6 " >
        <div class="card">
    <div class="card-image waves-effect waves-block waves-light">
      <img class="activator" src="img/Protezu-Prieziura-Ka-Svarbu-zinoti.jpg">
    </div>
    <div class="card-content taktai">
      <span class="card-title activator grey-text text-darken-4">Protezavimas<i class="material-icons right">more_vert</i></span>
      <p><a href="protezavimas.php">Plačiau</a></p>
    </div>
    <div class="card-reveal">
      <span class="card-title">Protezavimas<i class="material-icons right">close</i></span>
      <p>Protezavimas – tai danties kietųjų audinių anatominės formos ir spalvos atkūrimas, kai dantis dėl įgimtos ar įgytos patologijos suyra ir jo negalima atkurti plombinėmis medžiagomis.</p>
    </div>
    </div>
    </div>

</div>

</div>

<div class="parallax-container">
<div class="parallax gamta"><img src="img/irankiai.jpg"></div>
</div>


<?php include "footer.php"; ?>
</body>
</html>