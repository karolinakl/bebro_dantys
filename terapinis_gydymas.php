<!DOCTYPE html>
<html>
<head>	
	<title>Terapinis gydymas</title>

	<?php include "virsutiniai.php"; ?>
</head>
<body class="taktai">
<?php include "header.php"; ?>

<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li><a href="paslaugos.php">Paslaugos</a></li>
  <li>Terapinis gydymas</li>
</ul>

<h1 class="antraste">Terapinis gydymas (dantų plombavimas)</h1>
<div class="container ">


<div class="row">
    <div class="col s12 m12 l3">
    	<img class="responsive-img" src="img/priestg.jpg" alt="Dantys prieš plombavimą">
    	<img class="responsive-img" src="img/potg.jpg" alt="Dantys po plombavimo">
      
	</div>
	<div class="col s12 m12 l9">
		<p class="content-info">Terapinis gydymas (dantų plombavimas) taikomas tuomet, kai siekiama atstatyti pažeisto danties funkcijas bei estetinį vaizdą. Dažniausiai plombuoti tenka ėduonies pažeistus dantis. Tačiau tam tikrais atvejais plombavimas atliekamas ir siekiant suteikti patrauklesnį estetinį vaizdą neproporcingiems, nudilusiems, nuskilusiems ar pakeitusiems spalvą dantims (estetinis plombavimas). Laiku pritaikius terapinį gydymą ne tik išsaugomas nuosavas dantis, bet ir atkuriamos jo funkcijos bei nepriekaištingas estetinis vaizdas.</p>
	</div>
</div>

<h3>Dažniausiai užduodami klausimai</h3>
<hr class="linija">
<br>
 <ul class="collapsible">
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Kiek metų tarnauja plomba?</div>
      <div class="collapsible-body content-info-small"><span>Plomba gali tarnauti ir 10 metų. Tačiau vertėtų nepamiršti, kad plomba laikui bėgant keičia spalvą, gali aptrupėti. Todėl norint ilgiau išsaugoti plombavimo metu pasiektus rezultatus, pacientams kas 1,5 metų rekomenduojama apsilankyti pas gydytoją – odontologą.</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Ar dantų plombavimui taikomas garantinis laikotarpis?</div>
      <div class="collapsible-body content-info-small"><span>Dažniausiai plombos ilgaamžiškumas priklauso nuo paties paciento gyvenimo būdo, įpročių, burnos higienos bei vartojamų produktų. Dėl šių priežasčių garantijos estetiniam plombavimui kaip ir daugeliui kitų odontologinių procedūrų nėra taikomos.</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Kokie estetinio plombavimo privalumai?</div>
      <div class="collapsible-body content-info-small"><span>Pagrindinis estetinio plombavimo privalumas – estetinis vaizdas, kurio dėka pacientas gali džiaugtis gražia šypsena. Daugeliu atveju estetinis plombavimas kainuoja kur kas mažiau nei kiti galimi alternatyvūs problemos sprendimo būdai. Estetinio plombavimo dėka pacientas rezultatu gali džiaugtis vos po vieno ar kelių vizitų pas gydytoją – odontologą.
		</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Kokie yra estetinio plombavimo trūkumai?</div>
      <div class="collapsible-body content-info-small"><span>Estetinio plombavimo dėka per trumpą laiką Jūs vėl galėsite džiaugtis gražia ir sveika šypsena. Tačiau vertėtų nepamiršti, kad plombos dėvisi ir jas kas tam tikrą laiką privalu keisti naujomis. Šios procedūros metu neatstatomai nušlifuojami paciento natūralūs dantys.</span></div>
    </li>
  </ul>

</div>

<?php include "footer.php"; ?>
</body>
</html>