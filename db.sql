-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 22, 2019 at 04:23 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `bebro_dantys`
--

-- --------------------------------------------------------

--
-- Table structure for table `pacientu_duomenys`
--

CREATE TABLE `pacientu_duomenys` (
  `id` int(11) NOT NULL,
  `vardas` int(11) NOT NULL,
  `pavarde` int(11) NOT NULL,
  `gydymas` int(11) NOT NULL,
  `data` int(11) NOT NULL,
  `telefonas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pacientu_duomenys`
--
ALTER TABLE `pacientu_duomenys`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pacientu_duomenys`
--
ALTER TABLE `pacientu_duomenys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
