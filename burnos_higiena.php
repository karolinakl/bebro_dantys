<!DOCTYPE html>
<html>
<head>	
	<title>Burnos higiena</title>

	<?php include "virsutiniai.php"; ?>
</head>
<body class="taktai">
<?php include "header.php"; ?>

<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li><a href="paslaugos.php">Paslaugos</a></li>
  <li>Burnos higiena</li>
</ul>

<h1 class="antraste">Burnos higiena</h1>
<div class="container ">


<div class="row">
    <div class="col s12 m12 l6">
    	<img class="responsive-img" src="img/burnos-higiena.jpg" alt="Dantys prieš plombavimą">
    	<!-- <img class="responsive-img" src="" alt="Dantys po plombavimo"> -->
	</div>
	<div class="col s12 m12 l6">
		<p class="content-info">Profesionali burnos higienos procedūra atliekama odontologijos kabinete. Šios procedūros metu specialistas pašalina ne tik minkštas bei kietas apnašas, dantų akmenis, bet ir grąžina natūralų spindesį Jūsų dantims. Ši procedūra padeda panaikinti uždegimą, blogą burnos kvapą, dantenų kraujavimą bei dantų jautrumą.

Profesionali burnos higienos procedūra – prevencinė priemonė nuo dantų karieso, burnos gleivinės ir periodonto ligų – gingivito ir periodontito</p>
	</div>
</div>

<h3>Dažniausiai užduodami klausimai</h3>
<hr class="linija">
<br>
 <ul class="collapsible">
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Ar nuo rūkymo ir kavos vartojimo ant estetiškai plombuotų dantų atsiradusias apnašas galima pašalinti profesionalios burnos higienos procedūros metu?</div>
      <div class="collapsible-body content-info-small"><span>Taip, šias apnašas galima pašalinti ir nuo estetiškai plombuotų dantų, nepažeidžiant nei danties, nei plombos.</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Ar profesionali burnos higienos procedūra atliekama vaikams?</div>
      <div class="collapsible-body content-info-small"><span>Taip, ši procedūra atliekama ir mažiesiems pacientams, kuriems yra išdygę dantukai.</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Ar profesionali burnos higienos procedūra gali būti atliekama besilaukiančioms moterims?</div>
      <div class="collapsible-body content-info-small"><span>Taip, ši procedūra atliekama ir besilaukiančioms moterims.
		</span></div>
    </li>
    <li>
      <div class="collapsible-header content-info taktai"><i class="material-icons">question_answer</i>Ar profesionali burnos higienos procedūra skausminga?</div>
      <div class="collapsible-body content-info-small"><span>Įprastai profesionali burnos higienos procedūra neskausminga. Tačiau pacientams, turintiems jautrius dantis ar dantenas, ši procedūra gali sukelti nemalonių pojūčių.</span></div>
    </li>
  </ul>

</div>

<?php include "footer.php"; ?>
</body>
</html>