<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Bebro dantys</title>

  <?php include "virsutiniai.php"; ?>

</head>
<body class="taktai">
  <?php include "header.php"; ?>

  <div class=" content-info">
    <div class="slider">
      <ul class="slides">
        <li>
          <img src="img/default-dentist-3.jpg"> 
          <div class="caption center-align antraste-ant-pav">
            <h3 class="smile">Jūsų akinanti šypsena!</h3>
            <h5 class="light grey-text text-lighten-3">Balti, kaip bebro dantys</h5>
          </div>
        </li>
        <li>
          <img src="img/china_dent.jpg"> 
          <div class="caption left-align antraste-ant-pav">
            <h3 class="smile">Jokių pageltusių dantų!</h3>
            <h5 class="light grey-text text-lighten-3">Sveiki dantys!</h5>
          </div>
        </li>
        <li>
          <img src="img/images.jpeg">
          <div class="caption right-align antraste-ant-pav">
            <h3 class="smile">Džiaukitės bebro dantų baltumo šypsena!</h3>
            <h5 class="light grey-text text-lighten-3">Sveiki ir stiprūs dantys</h5>
          </div>
        </li>
      </div>
    </li>
  </ul>
<div class="container">
  <h3 class="antraste-be-fono"> Mes dirbame Jums!</h3>
  <br>
  <p>Jau šimtą aštuonesdešimt trejus su puse metų dirbame galvodami apie Jus!
    Mieli esami ir būsimi mūsų klientai,

    Mūsų gydytojai odontologai, pasitelkdami žinias ir sukauptą patirtį, pasirengę padaryti viską, kad padėtų išsaugoti ar susigrąžinti Jūsų gražią šypseną.

    Labai didelį dėmesį skiriame profilaktikai ir labai džiaugiamės, kai Jūs rūpinatės savo burnos sveikata ir pas specialistus atvykstate nelaukdami kol dantys pradės skaudėti. Juk dažnai reikia labai nedaug – tik laiku atlikti profesionalią burnos higieną, kurios metu galima pastebėjus gedimus ir labai lengvai juos sutvarkyti.

  Odontologijos klinikoje Bebro Dantys dirba stipri odontologų specialistų komanda. Visi kolektyvo nariai nuolat tobulinasi ir patys dalinasi patirtimi su kolegomis. Savo darbams keliame griežtus kokybės reikalavimus, naudojame pažangiausias technologijas, kurios, už didelius pinigus, pateisina net įnoringiausio kliento lūkesčius.</p>
  <br>
  <h3 class="antraste-be-fono">Mūsų paslaugos</h3>
  <br>
  <div class="row grid-test">
    <div class="col s12 m6 l3  cian">
     <a href="terapinis_gydymas.php" class="waves-effect waves-light btn-large index_mygtukai"><i class="material-icons left">local_hospital</i><span>Terapinis gydymas</span></a>
   </div>
   <div class="col s12 m6 l3  cian">
     <a href="chirurginis_gydymas.php" class="waves-effect waves-light btn-large index_mygtukai"><i class="material-icons left">restaurant</i><span>Chirurginis gydymas</span></a>
   </div>
   <div class="col s12 m6 l3  cian">
     <a href="burnos_higiena.php" class="waves-effect waves-light btn-large index_mygtukai"><i class="material-icons left">sentiment_very_satisfied</i><span>Burnos higiena</span></a>
   </div>
   <div class="col s12 m6 l3  cian">
     <a href="protezavimas.php" class="waves-effect waves-light btn-large index_mygtukai"><i class="material-icons left">settings</i><span>Protezavimas</span></a>
   </div>
 </div>
 </div>
</div>

<?php include "footer.php"; ?>
</body>
</html>