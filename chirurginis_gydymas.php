<!DOCTYPE html>
<html>
<head>	
	<title>Chirurginis gydymas</title>

	<?php include "virsutiniai.php"; ?>
</head>
<body class="taktai">
<?php include "header.php"; ?>

<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li><a href="paslaugos.php">Paslaugos</a></li>
  <li>Chirurginis gydymas</li>
</ul>

<h1 class="antraste">Chirurginis gydymas</h1>
<div class="container">



		<p class="content-info">Burnos chirurgija- specializuota odontologijos šaka, kurios metu burnos ertmėje atliekamos įvairios chirurginės procedūros. Gydytojo odontologo kompetencija įgalina atlikti tam tikrus chirurginius gydymo veiksmus, pvz. dantų ar jų šaknų pašalinimą, tačiau, esant sudėtingesniems atvejams, pacientas nukreipiamas specialisto konsultacijai ir gydymui.</p>


<h5>Klinikoje "Bebro dantys" atliekame šias chirurginias procedūras</h5>
<hr class="linija">
 <ul class="content-info">
    <li><i class="material-icons">star</i>Dantų šalinimas</li>
    <li><i class="material-icons">star</i>Retinuotų (protinių) dantų šalinimas</li>
    <li><i class="material-icons">star</i>Dantų šaknų rezekcijos</li>
    <li><i class="material-icons">star</i>Dantų šaknų amputacijos</li>
    <li><i class="material-icons">star</i>Dantų hemisekcijos (perskyrimas)</li>
    <li><i class="material-icons">star</i>Pūlinių atvėrimas</li>
    <li><i class="material-icons">star</i>Cistų šalinimo operacijos</li>
    <li><i class="material-icons">star</i>Liežuvio ir lūpos pasaitėlio plastika</li>  
  </ul>

</div>

<?php include "footer.php"; ?>
</body>
</html>