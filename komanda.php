<!DOCTYPE html>
<html>
<head>
				
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Bebro dantys</title>

	<?php include "virsutiniai.php"; ?>


</head>
<body class = "taktai">

<?php include "header.php"; ?>
	
<ul class="breadcrumbas">
  <li><a href="index.php">Bebro dantys</a></li>
  <li>Komanda</li>
</ul>


<h1 class = "antraste-be-fono"> Gydytojai Odontologai Specialistai </h1>
<div class="slick-slider content-info">
	<div><a class="center-item modal-trigger" href="#modal-one"> <p class = "deep-purple-text center">PAULIUS ANDRIJAUSKAS - Gydytojas odontologas</p><div class = "doctor-ph" style = "background-image: url(img/doctor.png);"></div></a></div>
	<div><a class="center-item modal-trigger" href="#modal-two"><p class="deep-purple-text center">JOLANTA BALČIŪNIENĖ - Gydytoja odontologė</p> <div class = "doctor-ph" style = "background-image: url(img/doctor2.jpg);"> </div></a></div> 
	<div><a class="center-item modal-trigger" href="#modal-three"><p class = "deep-purple-text center">DAINORA PEČIULIENĖ - Gydytoja odontologė</p><div class = "doctor-ph" style = "background-image: url(img/doctor3.jpg);"> </div></a></div> 
	<div><a class="center-item modal-trigger" href="#modal-four"><p class = "deep-purple-text center">VYTAUTAS KYGA - Gydytojas odontologas ortopedas</p><div class = "doctor-ph" style = "background-image: url(img/doctor4.jpg);"></div></a></div> 
</div>


  <div id="modal-one" class="modal taktai">
    <div class="modal-content">
      <h4>PAULIUS ANDRIJAUSKAS</h4>
      <h5>Gydytojas odontologas</h5>
      <p>Paulius Andrijauskas </p>
      <p>1999 m. – 2007 m. Nidos vidurinė mokykla.</p>
      <p>2007 m. – 2011 m. Kretingos Jurgio Pabrėžos gimnazija</p>
      <p>2011 m. – 2016 m. Odontologijos studijos Vilniaus universiteto medicinos fakulteto odontologijos institute.</p>
      <p>2016-07-01 – dabar. Gydytojas odontologas Vilniaus odontologijos klinika.</p>
      <p>2016-09-01 – dabar. Ortopedinės odontologijos gydytojas - rezidentas Vilniaus Universiteto ligoninės Žalgirio klinika.</p>
    </div>
    <div class="modal-footer">
    <a href="kontaktai.php" class="modal-close waves-effect waves-light btn-large deep-purple darken-4"><font color = "white">Registruotis pas gydytoją</font></a>
    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Uždaryti</a>
    </div>
  </div>

    <div id="modal-two" class="modal taktai">
    <div class="modal-content">
      <h4>JOLANTA BALČIŪNIENĖ</h4>
      <h5>Gydytoja odontologė</h5>
      <p>Gydytoja domisi vaiko psichologija, siekia užmegzti kontaktą su vaiku,</p>
      <p>paruošti jį dantų gydymo procedūroms, pasitelkiant įvairias žaidimo formas.</p>
      <p>Atlieka aiškinamąjį darbą tėveliams apie vaikų dantų ligų profilaktiką.</p>
      <p>Didelė kantrybė, 25 metų patirtis leidžia surasti bendrą kalbą su mažaisiais pacientais. </p>
      <p>Didžiausias darbo įvertinimas – nuoširdžios vaikų šypsenos. </p>
      <p>Specializacija: vaikų burnos ligų profilaktika, vaikų karieso gydymas,</p>
      <p>pieninių ir nuolatinių dantų šaknų kanalų gydymas, </p>
      <p>šaknų kanalų gydymas, pergydymas, naudojant mikroskopą, </p>
      <p>ltragarsinį skalerį, danties vainikų defektų atstatymas plombomis. </p>
    </div>
    <div class="modal-footer">
      <a href="kontaktai.php"" class="modal-close waves-effect waves-light btn-large deep-purple darken-4"> <font color = "white">Registruotis pas gydytoją</font></a>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Uždaryti</a>
    </div>
  </div>

    <div id="modal-three" class="modal taktai">
    <div class="modal-content">
      <h4>DAINORA PEČIULIENĖ</h4>
      <h5>Gydytoja odontologė</h5>
      <p>Daugiau kaip 20 metų patirtis, gydytojos kantrybė, ypatingas švelnumas,</p>
      <p>nuoširdumas ir rūpestingumas subūrė platų lojalių pacientų ratą.</p>
      <p>Gydytojos specializacija: </p>
      <p>šaknų kanalų gydymas, pergydymas, kietų šaknies kanalo užpildų išėmimas,</p>
      <p>naudojant mikroskopą, ultragarsinį skalerį,</p>
      <p>danties vainiko defektų atstatymas plombomis.</p>
    </div>
    <div class="modal-footer">
      <a href="kontaktai.php"" class="modal-close waves-effect waves-light btn-large deep-purple darken-4"><font color = "white">Registruotis pas gydytoją</font></a>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Uždaryti</a>
    </div>
  </div>

    <div id="modal-four" class="modal taktai">
    <div class="modal-content">
      <h4>VYTAUTAS KYGA</h4>
      <h5>Gydytojas odontologas ortopedas</h5>
      <p>25 metų profesinės praktikos patirtis</p>
      <p>protezuojant estetinėmis porceliano laminantėmis priekinius dantis,</p>
      <p>protezuojant sudėtingus klinikinius atvejus, periodontito pažeistus dantis, </p>
      <p> protezuojant įvairių rūšių protezais ant implantų.</p>
      <p>Gydytojas atlieka protezavimą įvairių rūšių šiuolaikiniais protezais:</p>
      <p>bemetalės keramikos, lietais, frezuotais ir kt.</p>
      <p>vainikėliais, tiltais, protezais ant implantų atramų.</p>
      <p>Taip pat protezais kombinacijoje su įvairiais fiksavimo protezais. </p>
      <p>Gydo pacientus, kenčiančius dėl funkcinių kramtymo sutrikimų.</p>
    </div>
    <div class="modal-footer">
      <a href="kontaktai.php"" class="modal-close waves-effect waves-light btn-large deep-purple darken-4"> <font color = "white">Registruotis pas gydytoją</font></a>
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Uždaryti</a>
    </div>
  </div>

<div class="container">
<br>
<p class="content-info"> Klinikoje dirbančių gydytojų odontologų, burnos higienistų, gydytojų odontologų padėjėjų tikslas - pasiūlyti mūsų klinikos pacientams aukščiausią paslaugų kokybę, leidžiančią užtikrinti burnos sveikatą. Odontologijos mokslui sparčiai žengiant pirmyn, mūsų specialistai nuolat gilina savo žinias Lietuvoje bei užsienyje, todėl savo pacientams galime pasiūlyti tai, kas šiandien naujausia ir geriausia pasaulyje.Klinikoje diegiamos naujos diagnostikos bei dantų gydymo technologijos leidžia pritaikyti naujausius gydymo metodus.</p>
 <br>
 <br>

<div class="row grid-test">
    <div class="col s12 m3 l3"> </div>
    <div class="col s12 m6 l6  cian">
      <a href ="vertinimas.php" class="waves-effect waves-light btn-large index_mygtukai" align = "justify"><i class="material-icons">rate_review</i><span>Mūsų paslaugų ir specialistų vertinimas</span></a>
    </div>
    <div class="col s12 m3 l3"></div>
</div>

 <br>
 <br>
<br>

 

</div>

<?php include "footer.php"; ?>
</body>

</html